FROM debian:latest
RUN apt-get -y update && apt-get install -y apt-utils gforth --fix-missing
COPY script.fs ./script.fs
ENTRYPOINT gforth ./script.fs
