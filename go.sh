#! /usr/bin/env bash

runner=$(which gforth-fast)
[ $(grep gforth-fast <<< ${runner}) ] && $runner ./script.fs > output.txt || ( echo "gforth not found" && exit 1 )
gzip output.txt
exit 0
